/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.blogspot.atifsoftwares.animatoo.slice;

import com.blogspot.atifsoftwares.animatoo.ResourceTable;
import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        DisplayAttributes displayAttributes = DisplayManager.getInstance()
                .getDefaultDisplay(this.getContext()).get().getAttributes();
        int screenWidth = displayAttributes.width;
        int screenHeight = displayAttributes.height;

        DirectionalLayout text1 = (DirectionalLayout) findComponentById(ResourceTable.Id_text1);
        DirectionalLayout text2 = (DirectionalLayout) findComponentById(ResourceTable.Id_text2);
        StackLayout parent = (StackLayout) findComponentById(ResourceTable.Id_textParent);
        Button reset = (Button) findComponentById(ResourceTable.Id_reset);
        DirectionalLayout buttonParent = (DirectionalLayout) findComponentById(ResourceTable.Id_buttonParent);


        Animatoo.setText1(text1);
        Animatoo.setText2(text2);
        Animatoo.setParent(parent);
        Animatoo.setScreenWidth(screenWidth);
        Animatoo.setScreenHeight(screenHeight);
        Animatoo.setButtonParent(buttonParent);

        reset.setClickedListener(component -> Animatoo.rebuild());
        // 页面跳转
        findComponentById(ResourceTable.Id_zoom).setClickedListener(component -> Animatoo.animateZoom(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_fade).setClickedListener(component -> Animatoo.animateFade(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_windmill).setClickedListener(component -> Animatoo.animateWindmill(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_spin).setClickedListener(component -> Animatoo.animateSpin(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_diagonal).setClickedListener(component -> Animatoo.animateDiagonal(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_split).setClickedListener(component -> Animatoo.animateSplit(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_shrink).setClickedListener(component -> Animatoo.animateShrink(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_card).setClickedListener(component -> Animatoo.animateCard(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_inAndOut).setClickedListener(component -> Animatoo.animateInAndOut(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_swipeLeft).setClickedListener(component -> Animatoo.animateSwipeLeft(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_swipeRight).setClickedListener(component -> Animatoo.animateSwipeRight(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_slideLeft).setClickedListener(component -> Animatoo.animateSlideLeft(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_slideRight).setClickedListener(component -> Animatoo.animateSlideRight(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_slideDown).setClickedListener(component -> Animatoo.animateSlideDown(MainAbilitySlice.this));
        findComponentById(ResourceTable.Id_slideUp).setClickedListener(component -> Animatoo.animateSlideUp(MainAbilitySlice.this));
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
