/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.blogspot.atifsoftwares.animatoolib;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorScatter;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class Animatoo {

    private static DirectionalLayout text1;
    private static DirectionalLayout text2;
    private static Animator animatorPropertyExit = null;
    private static Animator animatorPropertyEnter = null;
    private static StackLayout parent;
    private static int screenWidth;
    private static int screenHeight;
    private static DirectionalLayout buttonParent;

    public static void animateZoom(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_zoom_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .alphaFrom(0).alpha(1)
                .scaleXFrom(2f).scaleX(1f)
                .scaleYFrom(2f).scaleY(1f)
                .setCurveType(Animator.CurveType.DECELERATE)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_zoom_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .alphaFrom(1).alpha(0)
                .scaleXFrom(1f).scaleX(0.5f)
                .scaleYFrom(1f).scaleY(0.5f)
                .setCurveType(Animator.CurveType.DECELERATE)
                .start();
    }

    public static void animateFade(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_fade_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .alphaFrom(0).alpha(1)
                .setCurveType(Animator.CurveType.ACCELERATE)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_fade_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .alphaFrom(1).alpha(0)
                .setCurveType(Animator.CurveType.ACCELERATE)
                .start();
    }

    public static void animateWindmill(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_windmill_enter);
        text2.setRotation(180);
        text2.setPivot(0, 0);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .rotate(0)
                .alphaFrom(0).alpha(1)
                .scaleXFrom(0f).scaleX(1f)
                .scaleYFrom(0f).scaleY(1f)
                .setCurveType(Animator.CurveType.DECELERATE)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_windmill_exit);
        text1.setRotation(0);
        text1.setPivot(0, 0);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .rotate(-180)
                .alphaFrom(1).alpha(0)
                .scaleXFrom(1f).scaleX(0f)
                .scaleYFrom(1f).scaleY(0f)
                .setCurveType(Animator.CurveType.DECELERATE)
                .start();
    }

    public static void animateSpin(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_spin_enter);
        text2.setRotation(720);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .rotate(0)
                .alphaFrom(0).alpha(1)
                .scaleXFrom(0f).scaleX(1f)
                .scaleYFrom(0f).scaleY(1f)
                .setCurveType(Animator.CurveType.DECELERATE)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_spin_exit);
        text1.setRotation(0);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .rotate(720)
                .alphaFrom(1).alpha(0)
                .scaleXFrom(1f).scaleX(0f)
                .scaleYFrom(1f).scaleY(0f)
                .setCurveType(Animator.CurveType.DECELERATE)
                .start();
    }

    public static void animateDiagonal(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_diagonal_right_enter);
        ((AnimatorValue) animatorPropertyEnter).setValueUpdateListener((animatorValue, v) -> {
            text2.setAlpha(v);
            text2.setScale(v, v);
            text2.setContentPositionX(-screenWidth * (1 - v));
            text2.setContentPositionY(-screenHeight * (1 - v));
        });
        animatorPropertyEnter.start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_diagonal_right_exit);
        ((AnimatorValue) animatorPropertyExit).setValueUpdateListener((animatorValue, v) -> {
            text1.setAlpha(1 - v);
            text1.setScale(1 - v, 1 - v);
            double vx = (1 / ((Math.abs(Math.log(2 * v) / Math.log(Math.E)) + 3 * v) * 5)) - 0.5 * v * v;
            text1.setContentPositionX((float) (screenWidth * vx));
            text1.setContentPositionY((float) (screenHeight * vx));
        });
        animatorPropertyExit.start();
    }

    public static void animateSplit(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        parent.moveChildToFront(text2);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_split_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .scaleXFrom(0f).scaleX(1f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_split_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .scaleXFrom(1f).scaleX(0f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateShrink(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        parent.moveChildToFront(text2);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_shrink_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .scaleXFrom(0f).scaleX(1f)
                .scaleYFrom(0f).scaleY(1f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_shrink_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .scaleXFrom(1f).scaleX(0f)
                .scaleYFrom(1f).scaleY(0f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateCard(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        text2.setContentPositionX(-screenWidth);
        parent.moveChildToFront(text2);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_card_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .moveFromX(-screenWidth).moveToX(0)
                .setCurveType(Animator.CurveType.ACCELERATE_DECELERATE)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_card_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .scaleXFrom(1f).scaleX(0.5f)
                .scaleYFrom(1f).scaleY(0.5f)
                .alpha(0.5f)
                .setCurveType(Animator.CurveType.ACCELERATE_DECELERATE)
                .start();
    }

    public static void animateInAndOut(Context context) {
        setChildButtonClickable(false);
        ShapeElement shapeElement = (ShapeElement) parent.getBackgroundElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        parent.setBackground(shapeElement);
        parent.moveChildToFront(text2);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_in_out_enter);
        text2.setPivotX(screenWidth);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .scaleXFrom(0.5f).scaleX(1f)
                .scaleYFrom(0.5f).scaleY(1f)
                .alpha(1f).alphaFrom(0.5f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_in_out_exit);
        text1.setPivotX(0);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .scaleXFrom(1f).scaleX(0.5f)
                .scaleYFrom(1f).scaleY(0.5f)
                .alpha(0.5f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateSwipeLeft(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_swipe_left_enter);
        text2.setPivotX(screenWidth);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .scaleXFrom(0f).scaleX(1f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_swipe_left_exit);
        text1.setPivotX(0);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .scaleXFrom(1f).scaleX(0f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateSwipeRight(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_swipe_right_enter);
        text2.setPivotX(0);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .scaleXFrom(0f).scaleX(1f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_swipe_right_exit);
        text1.setPivotX(screenWidth);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .scaleXFrom(1f).scaleX(0f)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateSlideLeft(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_left_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .moveFromX(screenWidth).moveToX(0)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_left_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .moveFromX(0).moveToX(-screenWidth)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateSlideRight(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_in_left);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .moveFromX(-screenWidth).moveToX(0)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_out_right);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .moveFromX(0).moveToX(screenWidth)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateSlideDown(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_down_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .moveFromY(-screenHeight).moveToY(0)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_down_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .moveFromY(0).moveToY(screenHeight)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    public static void animateSlideUp(Context context) {
        setChildButtonClickable(false);
        animatorPropertyEnter = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_up_enter);
        ((AnimatorProperty) animatorPropertyEnter)
                .setTarget(text2)
                .moveFromY(screenHeight).moveToY(0)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
        animatorPropertyExit = AnimatorScatter.getInstance(context)
                .parse(ResourceTable.Animation_animate_slide_up_exit);
        ((AnimatorProperty) animatorPropertyExit)
                .setTarget(text1)
                .moveFromY(0).moveToY(-screenHeight)
                .setCurveType(Animator.CurveType.LINEAR)
                .start();
    }

    private static void setChildButtonClickable(boolean b) {
        int childCount = buttonParent.getChildCount();
        for (int i = 0; i < childCount; i++)
            buttonParent.getComponentAt(i).setClickable(b);

    }

    // 重置
    public static void rebuild() {
        setChildButtonClickable(true);
        parent.moveChildToFront(text1);
        text1.setScale(1f,1f);
        text2.setScale(1f,1f);
        text1.setAlpha(1f);
        text2.setAlpha(1f);
        text1.setContentPositionX(0);
        text1.setContentPositionY(0);
        text2.setContentPositionX(0);
        text2.setContentPositionY(0);
        text1.setRotation(0);
        text2.setRotation(0);
        text1.setPivot((float) screenWidth/2,(float) screenHeight/2);
        text2.setPivot((float) screenWidth/2,(float) screenHeight/2);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
        parent.setBackground(shapeElement);
    }

    // get\set方法
    public static DirectionalLayout getText1() {
        return text1;
    }

    public static void setText1(DirectionalLayout text1) {
        Animatoo.text1 = text1;
    }

    public static DirectionalLayout getText2() {
        return text2;
    }

    public static void setText2(DirectionalLayout text2) {
        Animatoo.text2 = text2;
    }

    public static StackLayout getParent() {
        return parent;
    }

    public static void setParent(StackLayout parent) {
        Animatoo.parent = parent;
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static void setScreenWidth(int screenWidth) {
        Animatoo.screenWidth = screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static void setScreenHeight(int screenHeight) {
        Animatoo.screenHeight = screenHeight;
    }

    public static DirectionalLayout getButtonParent() {
        return buttonParent;
    }

    public static void setButtonParent(DirectionalLayout buttonParent) {
        Animatoo.buttonParent = buttonParent;
    }
}