## Animatoo

### 项目介绍
一个轻量级、易于使用的 OpenHarmony库，提供了很棒的活动过渡动画

### 功能缺失
暂无法实现动画在页面跳转时实现, 页面跳转的自定义动画效果设置后无效果

###  安装教程

- 方式一：
 1. 下载模块代码添加到自己的工程
    
 2. 关联使用
 ```
 dependencies {
         implementation project(":animatoolib")
 ……
 }
 ```
 3. 在系统的settings.gradle中添加
 ```
 include ':animatoolib'
 ```


- 方式二：

```
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
    ...
    implementation 'com.gitee.archermind-ti:animatoo:1.0.1'
    ...
```

### 使用说明

- 使用 Animatoo

  只需在 startAbility(Intent intent, int requestCode) 之后添加一行短代码即可
  
  ***注意：*** **setTransitionAnimation(enterAnim，exitAnim）无法实现自定义页面跳转效果**
  
- 此库的所有可用方法：

  ```java
  Animatoo.animateZoom(context);
  Animatoo.animateFade(context);
  Animatoo.animateWindmill(context);
  Animatoo.animateSpin(context);
  Animatoo.animateDiagonal(context);
  Animatoo.animateSplit(context);
  Animatoo.animateShrink(context);
  Animatoo.animateCard(context);
  Animatoo.animateInAndOut(context);
  Animatoo.animateSwipeLeft(context);
  Animatoo.animateSwipeRight(context);
  Animatoo.animateSlideLeft(context);
  Animatoo.animateSlideRight(context);
  Animatoo.animateSlideDown(context);
  Animatoo.animateSlideUp(context);
  ```

### 效果演示

**windmill**

  ![image](screenshot/windmill.gif)

**diagonal**

  ![image](screenshot/diagonal.gif)

**card**

  ![image](screenshot/card.gif)

**剩下的效果**

  [screenshot](https://gitee.com/archermind-ti/animatoo/tree/master/srceenshot)

###  版本迭代

- v1.0.1

### License
 Animatoo 在 [MIT License](LICENSE)下获得许可
